var alumno={
    nombre:"",
    calificaciones:[],
    iniciarCalificaciones(){
        for (let i = 0; i < 10; i++) {
            this.calificaciones[i]=0;
        }          
    },
    mostrarCalificaciones(){
        stringCalificaciones="Calificaciones de "+this.nombre+":[";
        for (let i = 0; i < this.calificaciones.length; i++) {
            stringCalificaciones=stringCalificaciones+this.calificaciones[i]+",";
        }
        stringCalificaciones=stringCalificaciones+"]";
        return stringCalificaciones;
    },
    mediaCalificaciones(){
        let notaTotal=0;
        for (const nota of this.calificaciones) {
            notaTotal=notaTotal+nota;
        }
        return notaTotal/this.calificaciones.length;
    },
    anyadirCalicacion(calificacion){
        if((calificacion>=0)&&(calificacion<=10)) this.calificaciones.push(calificacion);
    },
    corregirCalificacion(nuevaCali,pos){
        if(nuevaCali>0){
            if(nuevaCali<11){
                this.calificaciones[pos]=nuevaCali;
            }
        }
    }
}